<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from trax.acrothemes.com/index-center-logo.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Jul 2021 11:03:28 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Trax | Center Logo</title>
    <link href="images/favicon.ico" rel="icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/tooltipster.min.css">
    <link rel="stylesheet" href="css/cubeportfolio.min.css">
    <link rel="stylesheet" href="css/revolution/navigation.css">
    <link rel="stylesheet" href="css/revolution/settings.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<!--PreLoader-->
<div class="loader">
    <div class="loader-inner">
        <div class="cssload-loader"></div>
    </div>
</div>
<!--PreLoader Ends-->
<!-- header -->
<header class="site-header" id="header">
    <nav class="navbar navbar-expand-lg  static-nav">
        <div class="container">
            <a class="navbar-brand center-brand" href="index.php">
                <img src="shop-11.jpg" alt="logo" class="logo-default">
                <img src="shop-11.jpg" alt="logo" class="logo-scrolled">
            </a>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item  static">
                        <a class="nav-link"  aria-haspopup="true" aria-expanded="false">     </a>
                        
                    </li>
                    <li class="nav-item  static">
                        <a class="nav-link"  aria-haspopup="true" aria-expanded="false">     </a>
                        
                    </li>
                    <li class="nav-item  static">
                        <a class="nav-link" href="index.php" aria-haspopup="true" aria-expanded="false"> Home </a>
                        
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.php">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="services.php">Services</a>
                    </li>
                    
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="portfolio.php">Portfolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog.php">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.php">Contact</a>
                    </li>
                    <li class="nav-item  static">
                        <a class="nav-link"  aria-haspopup="true" aria-expanded="false">     </a>
                        
                    </li>
                    <li class="nav-item">
                         <ul class="social-icons social-icons-simple d-lg-inline-block d-none animated fadeInUp mt-4" data-wow-delay="300ms">
                <li><a href="#."><i class="fab fa-facebook-f"></i> </a> </li>
                <li><a href="#."><i class="fab fa-instagram"></i> </a> </li>
                <li><a href="#."><i class="fab fa-twitter"></i> </a> </li>
                <li><a href="#."><i class="fab fa-linkedin-in"></i> </a> </li>
            </ul>
                    </li>

                </ul>
            </div>

        </div>
        <!--side menu open button-->
        <a href="javascript:void(0)" class="d-inline-block sidemenu_btn" id="sidemenu_toggle">
            <span class="bg-dark"></span> <span class="bg-dark"></span> <span class="bg-dark"></span>
        </a>
    </nav>
    <!-- side menu -->
    <div class="side-menu opacity-0 gradient-bg">
        <div class="overlay"></div>
        <div class="inner-wrapper">
            <span class="btn-close btn-close-no-padding" id="btn_sideNavClose"><i></i><i></i></span>
            <nav class="side-nav w-100">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link collapsePagesSideMenu" data-toggle="collapse" href="#sideNavPages1">
                            Home <i class="fas fa-chevron-down"></i>
                        </a>
                        <div id="sideNavPages1" class="collapse sideNavPages">
                            <ul class="navbar-nav mt-2">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.html">Standard Version</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-creative-agency.html">Creative Agency</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-light.html">Classic Light</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-video.html">Video Background</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-modern-agency.html">Modern Agency</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-classic-startup.html">Classic Startup</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-flat.html">Flat Version</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-innovative.html">Innovative Layout</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-one-page.html">One Page Layout</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="index-center-logo.html">Center Logo</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-bottom-nav.html">Bottom Nav</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-gray.html">Minimal Gray</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-parallax.html">Parallax Version</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-interactive-classic.html">Interactive Classic</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-design-studio.html">Design Studio</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index-particles.html">Interactive Particles</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.html">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery.html">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link collapsePagesSideMenu" data-toggle="collapse" href="#sideNavPages">
                            Pages <i class="fas fa-chevron-down"></i>
                        </a>
                        <div id="sideNavPages" class="collapse sideNavPages">
                            <ul class="navbar-nav mt-2">
                                <li class="nav-item">
                                    <a class="nav-link" href="team.html">Our Team</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="services.html">Service</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="services-detail.html">Service Detail</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="testimonial.html">Testimonials</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="gallery.html">Gallery</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="gallery-detail.html">Gallery Detail</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="pricing.html">Pricing</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="faq.html">FAQ's</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="404.html">Error 404</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="coming-soon.html">Coming Soon</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link collapsePagesSideMenu" data-toggle="collapse" href="#inner-2">
                                        Account <i class="fas fa-chevron-down"></i>
                                    </a>
                                    <div id="inner-2" class="collapse sideNavPages sideNavPagesInner">
                                        <ul class="navbar-nav mt-2">
                                            <li class="nav-item">
                                                <a class="nav-link" href="login.html">Login</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="register.html">Register</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="forget-password.html">Forget Password</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="support.html">Support</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link collapsePagesSideMenu" data-toggle="collapse" href="#inner-1">
                                        Shops <i class="fas fa-chevron-down"></i>
                                    </a>
                                    <div id="inner-1" class="collapse sideNavPages sideNavPagesInner">
                                        <ul class="navbar-nav mt-2">
                                            <li class="nav-item">
                                                <a class="nav-link" href="shop.html">Shop Products</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="shop-detail.html">Shop Detail</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="shop-cart.html">Shop Cart</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link collapsePagesSideMenu" data-toggle="collapse" href="#sideNavPages2">
                            Blogs <i class="fas fa-chevron-down"></i>
                        </a>
                        <div id="sideNavPages2" class="collapse sideNavPages">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="blog-1.html">Blog 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="blog-2.html">Blog 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="blog-detail.html">Blog Details</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.html">Contact</a>
                    </li>
                </ul>
            </nav>
            <div class="side-footer w-100">
                <ul class="social-icons-simple white top40">
                    <li><a href="javascript:void(0)"><i class="fab fa-facebook-f"></i> </a> </li>
                    <li><a href="javascript:void(0)"><i class="fab fa-twitter"></i> </a> </li>
                    <li><a href="javascript:void(0)"><i class="fab fa-instagram"></i> </a> </li>
                </ul>
                <p class="whitecolor">&copy; <span id="year"></span> Trax. Made With Love by ThemesIndustry</p>
            </div>
        </div>
    </div>
    <div id="close_side_menu" class="tooltip"></div>
    <!-- End side menu -->
</header>
<!-- header -->
