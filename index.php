<?php
include "header.php";
?>



<section id="home" class="p-0 h-100vh">
    <h2 class="d-none">heading</h2>
    <!--Main Slider-->
    <div id="rev_slider_8_1_wrapper" class="rev_slider_wrapper fullscreen-container transparent-bg" data-source="gallery">
        <!-- START REVOLUTION SLIDER 5.4.8.1 fullscreen mode -->
        <div id="rev_slider_8_1" class="rev_slider fullscreenbanner" data-version="5.4.8.1">
            <ul>
                <!-- SLIDE  -->
                <!-- SLIDE 1  -->
                <li data-index="rs-18" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="revolution/assets/100x50_59345-slider-bg.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/slider-bg.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-6 gradient-bg"
                         data-x="['left','left','left','left']" data-hoffset="['904','1245','1245','1245']"
                         data-y="['top','top','top','top']" data-voffset="['250','99','99','99']"
                         data-width="333"
                         data-height="455"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":80,"speed":1510,"frame":"0","from":"x:right;z:0;rX:0;rY:0;rZ:0;sX:0.7;sY:0.7;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 5; min-width: 333px; max-width: 455px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0;height:445px;width:331px;display:block;">
                        <div></div>
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                         data-x="['left','left','left','left']" data-hoffset="['754','1219','1219','1219']"
                         data-y="['top','top','top','top']" data-voffset="['364','187','187','187']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":80,"speed":1510,"frame":"0","from":"y:bottom;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 6;"><img src="images/slider-img1.jpg" alt="" data-ww="['331px','331px','331px','331px']" data-hh="['287px','287px','287px','287px']" data-no-retina> </div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption tp-resizeme color-summer-sky"
                         data-x="['left','center','center','center']" data-hoffset="['7','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['270','200','100','55']"
                         data-fontsize="['60','50','40','40']"
                         data-lineheight="['75','75','75','40']"
                         data-width="['556','556','556','300']"
                         data-height="['none','none','none','87']"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":100,"split":"no","splitdelay":0.1,"speed":1500,"split_direction":"farward","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"rgba(0,0,0,0)","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","color":"transparent","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 7; min-width: 556px; max-width: 556px; white-space: nowrap; font-size: 60px; line-height: 75px; font-weight: 700; letter-spacing: 0px;font-family: 'Poppins', sans-serif;">Your Success</div>
                    <!-- LAYER NR. 4 -->
                    <!-- <div class="tp-caption   tp-resizeme darkcolor"
                         data-x="['left','center','center','center']" data-hoffset="['8','0','0','0']"
                         data-y="['top','middle','middle','middle']" data-voffset="['460','95','0','10']"
                         data-whitespace="normal"
                         data-width="['656','650','550','440']"
                         data-fontsize="['16','15','15','15']"
                         data-lineheight="['22','22','22','22']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":100,"speed":1480,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 8; min-width: 646px; max-width: 646px; white-space: nowrap; font-size: 15px; line-height: 22px; font-weight: 400; letter-spacing: 0px;font-family: 'Poppins', sans-serif;">Lorem ipsum is simply dummy text of the printing and typesetting. Lorem Ipsum has been the industry’s standard dummy.  Lorem Ipsum has been the industry’s standard dummy.
                    </div> -->
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption   tp-resizeme"
                         id="slide-18-layer-3"
                         data-x="['left','left','left','left']" data-hoffset="['-331','-149','-162','-162']"
                         data-y="['top','top','top','top']" data-voffset="['28','-78','-127','-127']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:top;rZ:-360deg;opacity:0;","to":"o:1;","ease":"Power0.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 9;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="0" data-xe="0" data-ys="0" data-ye="120"><img src="images/slider-ele1.png" alt="" data-ww="['183px','183px','183px','183px']" data-hh="['445px','445px','445px','445px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['576','496','635','137']"
                         data-y="['top','top','top','top']" data-voffset="['258','439','274','299']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:bottom;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 10;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="0" data-xe="60" data-ys="100" data-ye="60"><img src="images/slider-ele2.png" alt="" data-ww="['51px','51px','51px','51px']" data-hh="['52px','52px','52px','52px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['1109','819','315','315']"
                         data-y="['top','top','top','top']" data-voffset="['160','149','340','293']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:middle;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 11;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="-90" data-xe="90" data-ys="0" data-ye="20"><img src="images/slider-ele3.png" alt="" data-ww="['50px','50px','50px','50px']" data-hh="['14px','14px','14px','14px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['355','491','401','401']"
                         data-y="['top','top','top','top']" data-voffset="['227','65','36','36']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:right;y:bottom;rZ:360deg;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 12;">
                        <div class="rs-looped rs-rotate"  data-easing="Power0.easeIn" data-startdeg="0" data-enddeg="360" data-speed="15" data-origin="50% 50%"><img src="images/slider-ele4.png" alt="" data-ww="['67px','67px','67px','67px']" data-hh="['69px','69px','69px','69px']" data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['509','138','-49','18']"
                         data-y="['top','top','top','top']" data-voffset="['615','29','364','-62']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1490,"frame":"0","from":"x:center;y:top;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 13;">
                        <div class="rs-looped rs-wave"  data-speed="12" data-angle="90" data-radius="22px" data-origin="50% 50%"><img src="images/slider-ele5.png" alt="" data-ww="['68px','68px','68px','68px']" data-hh="['67px','67px','67px','67px']" data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 10 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['1307','940','940','940']"
                         data-y="['top','top','top','top']" data-voffset="['606','421','421','421']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 14;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="0" data-xe="0" data-ys="10" data-ye="-100"><img src="images/slider-ele6.png" alt="" data-ww="['24px','24px','24px','24px']" data-hh="['87px','87px','87px','87px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 11 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['83','66','429','429']"
                         data-y="['top','top','top','top']" data-voffset="['753','489','471','471']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 15;">
                        <div class="rs-looped rs-wave"  data-speed="10" data-angle="90" data-radius="12px" data-origin="50% 50%"><img src="images/slider-ele7.png" alt="" data-ww="['44px','44px','44px','44px']" data-hh="['87px','87px','87px','87px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 12 -->
                   <!--  <div class="tp-caption tp-resizeme"
                         data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['170','200','90','120']"
                         data-frames='[{"from":"y:50px;opacity:0;","speed":1000,"to":"o:1;","delay":800,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-width="['320','320','320','320']"
                         style="z-index:99; max-width: 960px">
                        <a href="#our-feature" class="pagescroll border button-padding font-13 button btn-dark transition-3">Get Started</a>
                        <a href="javascript:void(0)" class="button border btn-primary button-padding font-13 transition-3">Learn More</a>
                    </div> -->
                    <!-- LAYER NR. 13 -->
                    <div class="tp-caption   tp-resizeme darkcolor"
                         data-x="['left','center','center','center']" data-hoffset="['7','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['348','260','155','98']"
                         data-fontsize="['60','50','40','40']"
                         data-lineheight="['75','75','45','45']"
                         data-width="['556','556','556','']"
                         data-height="['none','none','none','87']"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":100,"split":"chars","splitdelay":0.1,"speed":1480,"split_direction":"random","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"rgba(0,0,0,0)","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","color":"transparent","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 17; min-width: 556px; max-width: 556px; white-space: nowrap; font-size: 60px; line-height: 75px; font-weight: 700; letter-spacing: 0px;font-family: 'Poppins', sans-serif;"> Our Achievement </div>
                </li>
                <!-- SLIDE 2 -->
                <li data-index="rs-32" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="revolution/assets/100x50_59345-slider-bg.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/slider-bg.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                         data-x="['left','left','left','left']" data-hoffset="['0','1219','1219','1219']"
                         data-y="['top','top','top','top']" data-voffset="['320','187','187','187']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":80,"speed":1510,"frame":"0","from":"y:bottom;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 6;"><img src="images/laptop.png" alt="" data-ww="['550px','550px','550px','550px']" data-hh="['auto','auto','auto','auto']" data-no-retina> </div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption tp-resizeme color-summer-sky"
                         data-x="['right','center','center','center']" data-hoffset="['-295','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['270','200','100','55']"
                         data-fontsize="['60','50','40','40']"
                         data-lineheight="['75','75','75','40']"
                         data-width="['556','556','556','300']"
                         data-height="['none','none','none','87']"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":100,"split":"no","splitdelay":0.1,"speed":1500,"split_direction":"farward","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"rgba(0,0,0,0)","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","color":"transparent","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 7; min-width: 556px; max-width: 556px; white-space: nowrap; font-size: 60px; line-height: 75px; font-weight: 700; letter-spacing: 0px;font-family: 'Poppins', sans-serif;">Your Business</div>
                    <!-- LAYER NR. 4 -->
                    <!-- <div class="tp-caption   tp-resizeme color-black"
                         data-x="['right','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','middle','middle','middle']" data-voffset="['460','95','0','10']"
                         data-whitespace="normal"
                         data-width="['656','650','550','440']"
                         data-fontsize="['16','15','15','15']"
                         data-lineheight="['22','22','22','22']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":100,"speed":1480,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['right','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 8; min-width: 646px; max-width: 646px; white-space: nowrap; font-size: 15px; line-height: 22px; font-weight: 400; letter-spacing: 0px;font-family: 'Poppins', sans-serif;">Lorem ipsum is simply dummy text of the printing and typesetting. Lorem Ipsum has been the industry’s standard dummy.  Lorem Ipsum has been the industry’s standard dummy.
                    </div> -->
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['-331','-149','-162','-162']"
                         data-y="['top','top','top','top']" data-voffset="['28','-78','-127','-127']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:top;rZ:-360deg;opacity:0;","to":"o:1;","ease":"Power0.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 9;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="0" data-xe="0" data-ys="0" data-ye="120"><img src="images/slider-ele1.png" alt="" data-ww="['183px','183px','183px','183px']" data-hh="['445px','445px','445px','445px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['576','496','635','137']"
                         data-y="['top','top','top','top']" data-voffset="['258','439','274','299']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:bottom;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 10;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="0" data-xe="60" data-ys="100" data-ye="60"><img src="images/slider-ele2.png" alt="" data-ww="['51px','51px','51px','51px']" data-hh="['52px','52px','52px','52px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['1109','819','315','315']"
                         data-y="['top','top','top','top']" data-voffset="['160','149','340','293']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:middle;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 11;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="-90" data-xe="90" data-ys="0" data-ye="20"><img src="images/slider-ele3.png" alt="" data-ww="['50px','50px','50px','50px']" data-hh="['14px','14px','14px','14px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['355','491','401','401']"
                         data-y="['top','top','top','top']" data-voffset="['227','65','36','36']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:right;y:bottom;rZ:360deg;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 12;">
                        <div class="rs-looped rs-rotate"  data-easing="Power0.easeIn" data-startdeg="0" data-enddeg="360" data-speed="15" data-origin="50% 50%"><img src="images/slider-ele4.png" alt="" data-ww="['67px','67px','67px','67px']" data-hh="['69px','69px','69px','69px']" data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['509','138','-49','18']"
                         data-y="['top','top','top','top']" data-voffset="['615','29','364','-62']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1490,"frame":"0","from":"x:center;y:top;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 13;">
                        <div class="rs-looped rs-wave"  data-speed="12" data-angle="90" data-radius="22px" data-origin="50% 50%"><img src="images/slider-ele5.png" alt="" data-ww="['68px','68px','68px','68px']" data-hh="['67px','67px','67px','67px']" data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 10 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['1307','940','940','940']"
                         data-y="['top','top','top','top']" data-voffset="['606','421','421','421']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 14;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="0" data-xe="0" data-ys="10" data-ye="-100"><img src="images/slider-ele6.png" alt="" data-ww="['24px','24px','24px','24px']" data-hh="['87px','87px','87px','87px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 11 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['83','66','429','429']"
                         data-y="['top','top','top','top']" data-voffset="['753','489','471','471']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 15;">
                        <div class="rs-looped rs-wave"  data-speed="10" data-angle="90" data-radius="12px" data-origin="50% 50%"><img src="images/slider-ele7.png" alt="" data-ww="['44px','44px','44px','44px']" data-hh="['87px','87px','87px','87px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 12 -->
                   <!--  <div class="tp-caption NotGeneric-Title tp-resizeme font-weight-100 text-white"
                         data-x="['right','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['170','200','90','120']"
                         data-frames='[{"from":"y:50px;opacity:0;","speed":1000,"to":"o:1;","delay":800,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);bw:2px 2px 2px 2px;"}]'
                         data-textAlign="['right','center','center','center']"
                         data-width="['160','160','160','160']"
                         style="z-index:99; max-width: 960px">
                        <a href="javascript:void(0)" class="button btn-primary transition-3 border button-padding font-13">Learn More</a>
                    </div> -->
                    <!-- LAYER NR. 13 -->
                    <div class="tp-caption   tp-resizeme darkcolor"
                         data-x="['right','center','center','center']" data-hoffset="['-135','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['348','260','155','98']"
                         data-fontsize="['60','50','40','40']"
                         data-lineheight="['75','75','45','45']"
                         data-width="['556','556','556','350']"
                         data-height="['none','none','none','87']"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":100,"split":"chars","splitdelay":0.1,"speed":1480,"split_direction":"random","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"rgba(0,0,0,0)","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","color":"transparent","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 17; min-width: 556px; max-width: 556px; white-space: nowrap; font-size: 60px; line-height: 75px; font-weight: 700; letter-spacing: 0px;font-family: 'Poppins', sans-serif;">Our Effort</div>
                </li>
                <!-- SLIDE 3 -->
                <li data-index="rs-36" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="revolution/assets/100x50_59345-slider-bg.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/slider-bg.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption tp-resizeme color-summer-sky"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['270','200','100','55']"
                         data-fontsize="['60','50','40','40']"
                         data-lineheight="['75','75','75','40']"
                         data-width="['556','556','556','300']"
                         data-height="['none','none','none','87']"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":100,"split":"no","splitdelay":0.1,"speed":1500,"split_direction":"farward","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"rgba(0,0,0,0)","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","color":"transparent","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 7; min-width: 556px; max-width: 556px; white-space: nowrap; font-size: 60px; line-height: 75px; font-weight: 700; letter-spacing: 0px;font-family: 'Poppins', sans-serif;">Fastest Way</div>
                    <!-- LAYER NR. 4 -->
                    <!-- <div class="tp-caption   tp-resizeme darkcolor"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','middle','middle','middle']" data-voffset="['460','95','0','10']"
                         data-whitespace="normal"
                         data-width="['656','650','550','440']"
                         data-fontsize="['16','15','15','15']"
                         data-lineheight="['22','22','22','22']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":100,"speed":1480,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 8; min-width: 646px; max-width: 646px; white-space: nowrap; font-size: 15px; line-height: 22px; font-weight: 400; letter-spacing: 0px;font-family: 'Poppins', sans-serif;">Lorem ipsum is simply dummy text of the printing and typesetting. Lorem Ipsum has been the industry’s standard dummy.  Lorem Ipsum has been the industry’s standard dummy.
                    </div> -->
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['-331','-149','-162','-162']"
                         data-y="['top','top','top','top']" data-voffset="['28','-78','-127','-127']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:top;rZ:-360deg;opacity:0;","to":"o:1;","ease":"Power0.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 9;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="0" data-xe="0" data-ys="0" data-ye="120"><img src="images/slider-ele1.png" alt="" data-ww="['183px','183px','183px','183px']" data-hh="['445px','445px','445px','445px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['100','496','635','137']"
                         data-y="['top','top','top','top']" data-voffset="['300','439','274','299']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:bottom;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 10;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="0" data-xe="60" data-ys="100" data-ye="60"><img src="images/slider-ele2.png" alt="" data-ww="['51px','51px','51px','51px']" data-hh="['52px','52px','52px','52px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['1109','819','315','315']"
                         data-y="['top','top','top','top']" data-voffset="['160','149','340','293']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:middle;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 11;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="-90" data-xe="90" data-ys="0" data-ye="20"><img src="images/slider-ele3.png" alt="" data-ww="['50px','50px','50px','50px']" data-hh="['14px','14px','14px','14px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['250','491','401','401']"
                         data-y="['top','top','top','top']" data-voffset="['227','65','36','36']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:right;y:bottom;rZ:360deg;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 12;">
                        <div class="rs-looped rs-rotate"  data-easing="Power0.easeIn" data-startdeg="0" data-enddeg="360" data-speed="15" data-origin="50% 50%"><img src="images/slider-ele4.png" alt="" data-ww="['67px','67px','67px','67px']" data-hh="['69px','69px','69px','69px']" data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['850','138','-49','18']"
                         data-y="['top','top','top','top']" data-voffset="['615','29','364','-62']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1490,"frame":"0","from":"x:center;y:top;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 13;">
                        <div class="rs-looped rs-wave"  data-speed="12" data-angle="90" data-radius="22px" data-origin="50% 50%"><img src="images/slider-ele5.png" alt="" data-ww="['68px','68px','68px','68px']" data-hh="['67px','67px','67px','67px']" data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 10 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['1307','940','940','940']"
                         data-y="['top','top','top','top']" data-voffset="['606','421','421','421']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 14;">
                        <div class="rs-looped rs-slideloop"  data-easing="Power0.easeInOut" data-speed="12" data-xs="0" data-xe="0" data-ys="10" data-ye="-100"><img src="images/slider-ele6.png" alt="" data-ww="['24px','24px','24px','24px']" data-hh="['87px','87px','87px','87px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 11 -->
                    <div class="tp-caption   tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="['83','66','429','429']"
                         data-y="['top','top','top','top']" data-voffset="['753','489','471','471']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="image"
                         data-responsive_offset="on"
                         data-frames='[{"delay":50,"speed":1480,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 15;">
                        <div class="rs-looped rs-wave"  data-speed="10" data-angle="90" data-radius="12px" data-origin="50% 50%"><img src="images/slider-ele7.png" alt="" data-ww="['44px','44px','44px','44px']" data-hh="['87px','87px','87px','87px']"  data-no-retina> </div>
                    </div>
                    <!-- LAYER NR. 12 -->
                    <!-- <div class="tp-caption tp-resizeme"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['170','200','90','120']"
                         data-frames='[{"from":"y:50px;opacity:0;","speed":1000,"to":"o:1;","delay":800,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);bw:2px 2px 2px 2px;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-width="['160','160','160','160']"
                         style="z-index:99; max-width: 960px">
                        <a href="javascript:void(0)" class="button btn-primary transition-3 border button-padding font-13">Read More</a>
                    </div> -->
                    <!-- LAYER NR. 13 -->
                    <div class="tp-caption   tp-resizeme darkcolor"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['348','260','155','98']"
                         data-fontsize="['60','50','40','40']"
                         data-lineheight="['75','75','45','45']"
                         data-width="['556','556','556','350']"
                         data-height="['none','none','none','87']"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":100,"split":"chars","splitdelay":0.1,"speed":1480,"split_direction":"random","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"rgba(0,0,0,0)","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":350,"frame":"999","color":"transparent","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 17; min-width: 556px; max-width: 556px; white-space: nowrap; font-size: 60px; line-height: 75px; font-weight: 700; letter-spacing: 0px;font-family: 'Poppins', sans-serif;">To Acheive Success</div>
                </li>
            </ul>
            <div class="tp-bannertimer" style="height: 5px; background: rgba(0,0,0,0.15);"></div>
        </div>
    </div>
    <!--Main Slider ends -->
</section>
<!-- Main Section end -->



<!--Some Feature -->
<section id="our-feature" class="single-feature padding">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-lg-6 col-md-7 col-sm-7 text-sm-left text-center wow fadeInLeft" data-wow-delay="300ms">
                <div class="heading-title mb-4">
                    <h2 class="darkcolor font-normal bottom30">Lets take your <span class="defaultcolor">Business</span> to Next Level</h2>
                </div>
                <p class="bottom35">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mauris arcu, lobortis id interdum vitae, interdum eget elit. Curabitur quis urna nulla. Suspendisse potenti. Duis suscipit ultrices maximus. </p>
                <a href="#our-team" class="button btnsecondary gradient-btn pagescroll mb-sm-0 mb-4">Learn More</a>
            </div>
            <div class="col-lg-5 offset-lg-1 col-md-5 col-sm-5 wow fadeInRight" data-wow-delay="300ms">
                <div class="image"><img alt="SEO" src="images/awesome-feature.png"></div>
            </div>
        </div>
    </div>
</section>
<!--Some Feature ends-->




<!-- WOrk Process-->
<section id="our-process" class="padding bgdark">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <div class="heading-title whitecolor wow fadeInUp" data-wow-delay="300ms">
                    <!-- <span>Quisque tellus risus, adipisci </span> -->
                    <h2 class="font-normal">Our Work Process </h2>
                </div>
            </div>
        </div>
        <div class="row">
            <ul class="process-wrapp">
                <li class="whitecolor wow fadeIn" data-wow-delay="300ms">
                    <span class="pro-step bottom20">01</span>
                    <p class="fontbold bottom25">Concept</p>
                    <p class="mt-n2 mt-sm-0">Quisque tellus risus, adipisci viverra bibendum urna.</p>
                </li>
                <li class="whitecolor wow fadeIn" data-wow-delay="400ms">
                    <span class="pro-step bottom20">02</span>
                    <p class="fontbold bottom25">Plan</p>
                    <p class="mt-n2 mt-sm-0">Quisque tellus risus, adipisci viverra bibendum urna.</p>
                </li>
                <li class="whitecolor wow fadeIn" data-wow-delay="500ms">
                    <span class="pro-step bottom20">03</span>
                    <p class="fontbold bottom25">Design</p>
                    <p class="mt-n2 mt-sm-0">Quisque tellus risus, adipisci viverra bibendum urna.</p>
                </li>
                <li class="whitecolor wow fadeIn" data-wow-delay="600ms">
                    <span class="pro-step bottom20">04</span>
                    <p class="fontbold bottom25">Development</p>
                    <p class="mt-n2 mt-sm-0">Quisque tellus risus, adipisci viverra bibendum urna.</p>
                </li>
                <li class="whitecolor wow fadeIn" data-wow-delay="700ms">
                    <span class="pro-step bottom20">05</span>
                    <p class="fontbold bottom25">Quality Check</p>
                    <p class="mt-n2 mt-sm-0">Quisque tellus risus, adipisci viverra bibendum urna.</p>
                </li>
            </ul>
        </div>
    </div>
</section>
<!--WOrk Process ends-->


<!-- Mobile Apps -->
<section id="our-apps" class="padding">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-7 col-sm-12">
                <div class="heading-title bottom30 wow fadeInUp" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
                    <span class="defaultcolor text-center text-md-left">Quisque tellus risus, adipisci viverra</span>
                    <h2 class="darkcolor font-normal text-center text-md-left">Mobile App Designs</h2>
                </div>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-12">
                <p class="text-center text-md-left">Curabitur mollis bibendum luctus. Duis suscipit vitae dui sed suscipit. Vestibulum auctor nunc vitae diam eleifend, in maximus metus sollicitudin. Quisque vitae sodales lectus. </p>
            </div>
        </div>
        <div class="row d-flex align-items-center" id="app-feature">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="text-center text-md-right">
                    <div class="feature-item mt-3 wow fadeInLeft" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInLeft;">
                        <div class="icon"><i class="fas fa-cog"></i></div>
                        <div class="text">
                            <h3 class="bottom15">
                                <span class="d-inline-block">Theme Options</span>
                            </h3>
                            <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet</p>
                        </div>
                    </div>
                    <div class="feature-item mt-5 wow fadeInLeft" data-wow-delay="350ms" style="visibility: visible; animation-delay: 350ms; animation-name: fadeInLeft;">
                        <div class="icon"><i class="fas fa-edit"></i></div>
                        <div class="text">
                            <h3 class="bottom15">
                                <span class="d-inline-block">Customization</span>
                            </h3>
                            <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="app-image top30">
                    <div class="app-slider-lock-btn"></div>
                    <div class="app-slider-lock">
                        <img src="images/iphone-slide-lock.jpg" alt="">
                    </div>
                    <div id="app-slider" class="owl-carousel owl-theme owl-loaded owl-drag">



                        <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-470px, 0px, 0px); transition: all 0s ease 0s; width: 1645px;"><div class="owl-item cloned" style="width: 235px;"><div class="item">
                            <img src="images/iphone-slide2.jpg" alt="">
                        </div></div><div class="owl-item cloned" style="width: 235px;"><div class="item">
                            <img src="images/iphone-slide3.jpg" alt="">
                        </div></div><div class="owl-item active" style="width: 235px;"><div class="item">
                            <img src="images/iphone-slide1.jpg" alt="">
                        </div></div><div class="owl-item" style="width: 235px;"><div class="item">
                            <img src="images/iphone-slide2.jpg" alt="">
                        </div></div><div class="owl-item" style="width: 235px;"><div class="item">
                            <img src="images/iphone-slide3.jpg" alt="">
                        </div></div><div class="owl-item cloned" style="width: 235px;"><div class="item">
                            <img src="images/iphone-slide1.jpg" alt="">
                        </div></div><div class="owl-item cloned" style="width: 235px;"><div class="item">
                            <img src="images/iphone-slide2.jpg" alt="">
                        </div></div></div></div><div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button></div><div class="owl-dots disabled"></div></div>
                    <img src="images/iphone.png" alt="image">
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="text-center text-md-left">
                    <div class="feature-item mt-3 wow fadeInRight" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInRight;">
                        <div class="icon"><i class="fas fa-code"></i></div>
                        <div class="text">
                            <h3 class="bottom15">
                                <span class="d-inline-block">Powerful Code</span>
                            </h3>
                            <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet</p>
                        </div>
                    </div>
                    <div class="feature-item mt-5 wow fadeInRight" data-wow-delay="350ms" style="visibility: visible; animation-delay: 350ms; animation-name: fadeInRight;">
                        <div class="icon"><i class="far fa-folder-open"></i></div>
                        <div class="text">
                            <h3 class="bottom15">
                                <span class="d-inline-block">Documentation </span>
                            </h3>
                            <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Mobile Apps ends-->


<!-- Counters -->
<section id="bg-counters" class="padding bg-counters parallax">
    <div class="container">
        <div class="row align-items-center text-center">
            <div class="col-lg-4 col-md-4 col-sm-4 bottom10">
                <div class="counters whitecolor  top10 bottom10">
                    <span class="count_nums font-light" data-to="20" data-speed="2500"> </span>
                </div>
                <h3 class="font-light whitecolor top20">Since We Started</h3>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <p class="whitecolor top20 bottom20 font-light title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mauris arcu, lobortis id interdum vitae, interdum eget elit. </p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 bottom10">
                <div class="counters whitecolor top10 bottom10">
                    <span class="count_nums font-light" data-to="2" data-speed="2500"> </span>
                </div>
                <h3 class="font-light whitecolor top20">Since We Started</h3>
            </div>
        </div>
    </div>
</section>
<!-- Counters ends-->


<!-- Partners-->
<section id="our-partners" class="padding">
    <div class="container">
        <div class="row">
            <h2 class="d-none">Partners Carousel</h2>
            <div class="col-md-12 col-sm-12">
                <div id="partners-slider" class="owl-carousel">
                    <div class="item">
                        <div class="logo-item"> <img alt="" src="images/logo-1.png"></div>
                    </div>
                    <div class="item">
                        <div class="logo-item"><img alt="" src="images/logo-2.png"></div>
                    </div>
                    <div class="item">
                        <div class="logo-item"><img alt="" src="images/logo-3.png"></div>
                    </div>
                    <div class="item">
                        <div class="logo-item"><img alt="" src="images/logo-4.png"></div>
                    </div>
                    <div class="item">
                        <div class="logo-item"><img alt="" src="images/logo-5.png"></div>
                    </div>
                    <div class="item">
                        <div class="logo-item"><img alt="" src="images/logo-1.png"></div>
                    </div>
                    <div class="item">
                        <div class="logo-item"><img alt="" src="images/logo-2.png"></div>
                    </div>
                    <div class="item">
                        <div class="logo-item"><img alt="" src="images/logo-3.png"></div>
                    </div>
                    <div class="item">
                        <div class="logo-item"><img alt="" src="images/logo-4.png"></div>
                    </div>
                    <div class="item">
                        <div class="logo-item"><img alt="" src="images/logo-5.png"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Partners ends-->



<?php
include "footer.php";
?>